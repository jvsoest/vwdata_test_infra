#######################################
# requires Crypto library
# install using "pip install pycrypto"
#######################################

from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import base64

def generatePrivateKey(multiplierBits):
    privateKey = RSA.generate(2048*multiplierBits)
    return privateKey

def generatePublicKey(privateKey):
    return privateKey.publickey()

def exportKey(key, fileName):
    f = open(fileName, 'w')
    f.write(key.exportKey('PEM').decode("UTF-8"))
    f.close()

def importKey(fileName):
    f = open(fileName, 'r')
    key = RSA.importKey(f.read())
    return key

def encrypt(text, publicKey):
    if publicKey.has_private():
        raise Exception("Private key used for encryption")
    
    cipher = PKCS1_OAEP.new(publicKey)
    encrypted = cipher.encrypt(text.encode())
    encryptedBase64 = base64.b64encode(encrypted).decode("UTF-8")
    return encryptedBase64

def decrypt(encryptedString, privateKey):
    if not privateKey.has_private():
        raise Exception("Wrong key used for decryption, please use private key")
    encrypted = base64.b64decode(encryptedString)
    cipher = PKCS1_OAEP.new(privateKey)
    decryptedText = cipher.decrypt(encrypted).decode("UTF-8")
    return decryptedText

##############################################################################

# generate private key
privateKey = generatePrivateKey(1)
# generate public key
publicKey = generatePublicKey(privateKey)

# save keys
exportKey(privateKey, "private.pem")
exportKey(publicKey, "public.pem")

##############################################################################

#import/read keys
privateKey = importKey("private.pem")
publicKey = importKey("public.pem")

# test encryption and decryption
testString = "Hello World!"
encrypted = encrypt(testString, publicKey)
decrypted = decrypt(encrypted, privateKey)

testString == decrypted